const std = @import("std");

fn setupLinks(step: *std.Build.Step.Compile) void {
    step.linkSystemLibrary("c");

    step.linkSystemLibrary("lilv-0");
    step.linkSystemLibrary("sndfile");
    step.linkSystemLibrary("readline");

    step.linkSystemLibrary("GraphicsMagickWand");
    step.linkSystemLibrary("GraphicsMagick");

    step.addIncludePath(.{ .path = "/usr/include/GraphicsMagick" });
    step.addIncludePath(.{ .path = "/usr/include" });

    const possible_lilv_include_dirs = [_][]const u8{
        "/usr/include/lilv-0/lilv",
        "/usr/include/lilv-0",
    };

    var found_any_lilv = false;

    for (possible_lilv_include_dirs) |possible_lilv_dir| {
        var possible_dir = std.fs.cwd().openDir(possible_lilv_dir, .{}) catch |err| {
            std.debug.print("possible lilv {s} fail: {s}\n", .{ possible_lilv_dir, @errorName(err) });
            continue;
        };

        possible_dir.close();
        found_any_lilv = true;

        std.debug.print("found lilv at '{s}'\n", .{possible_lilv_dir});
        step.addIncludePath(.{ .path = possible_lilv_dir });
    }

    if (!found_any_lilv) {
        std.debug.print("No LILV library was found :(\n", .{});
        @panic("no lilv found");
    }
}

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "scritcher",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });
    setupLinks(exe);
    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const test_step = b.addTest(.{
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });
    setupLinks(test_step);
    const run_unit_tests = b.addRunArtifact(test_step);
    const test_cmd = b.step("test", "run unit tests");
    test_cmd.dependOn(&run_unit_tests.step);
}
