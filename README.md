scritcher
====

glitch art "framework", ???????? language??? something?

![example of the amp command](https://estrogen.fun/i/57y7kn55.png)

![example of the rflanger command](https://estrogen.fun/i/i2055hdf.png)

# how do?

## build depedencies (headers included):
 - zig at https://ziglang.org
 - libc, lilv and libsndfile
 - graphicsmagick for the `rotate` command
 - readline (for repl)

## plugin depedencies (only required at runtime):
 - lv2 default plugins (most specifically the eg-amp plugin,
    will likely be there by default)
 - the SWH plugins ( https://github.com/swh/lv2 )
 - the Invada Studio plugins ( https://launchpad.net/invada-studio/ )
 - abGate plugin
 - MDA plugins
 - Calf plugins

```bash
# build and install
# assumes lilv headers are at /usr/include/lilv-0, edit build.zig if different
git clone https://gitdab.com/luna/scritcher.git
cd scritcher

zig build install --prefix ~/.local/

# on your input image file
convert blah.jpg blah.bmp

# if you got the lv2 default amp plugin, this is runnable
scritcher examples/middle_amp.scri blah.bmp

# if not, the noise plugin does not depend on any lv2 plugin.
scritcher examples/noise.scri blah.bmp

// scritcher saves it on an incremental basis, so the first run will save to
// blah_g1.bmp, the second saves to blah_g2.bmp, etc.
$your_image_viewer blah_g1.bmp
```

# using the repl

using repl works via `scritcher repl scri_file.scri input_image.bmp`

you type commands as you'd write the specific scritcher commands
(`doc/README.md`), with four repl-specific ones (semicolons do not apply):
 - `push`, to push the last written command to the queue
 - `save`, to write the queue to the given `scri_file.scri` file
 - `list`, to print the current contents of the queue
 - `quit`, to exit

After a non-REPL command, such as an effect, the program pointed by
`SCRITCHER_RUNNER` will run as argument to the `runqs` command. By default,
the program run will be `ristretto` (as it is my preffered image viewer,
considering it was able to handle when some images went broke)

this allows for quicker iteration of commands, as you can type a command, see
the image changes as fast as possible, tweak its arguments,
and when satisfied, `push` it, and work on the next command, etc.
