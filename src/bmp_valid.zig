const std = @import("std");
const log = std.log.scoped(.scritcher_bmp);
pub const BMPValidError = error{InvalidMagic};

const VALID_MAGICS = [_][]const u8{
    "BM",
    "BA",
    "CI",
    "CP",
    "IC",
    "PT",
};

pub fn magicValid(magic: []const u8) !void {
    var valid = false;

    for (VALID_MAGICS) |valid_magic| {
        if (std.mem.eql(u8, magic, valid_magic)) valid = true;
    }

    if (!valid) {
        log.debug("\tINVALID HEADER: '{s}'", .{magic});
        return BMPValidError.InvalidMagic;
    }
}
