const std = @import("std");
const langs = @import("lang.zig");

const log = std.log.scoped(.scritcher_printer);

fn printCommandWithParams(stream: anytype, command: anytype) !void {
    const Parameters = @TypeOf(command.parameters);
    try stream.print(" {d} {d}", .{ command.split, command.index });
    inline for (@typeInfo(Parameters).Struct.fields) |field| {
        if (field.type == f32 or field.type == f64) {
            try stream.print(" {}", .{@field(command.parameters, field.name)});
        } else if (field.type == usize or field.type == u64) {
            try stream.print(" {d}", .{@field(command.parameters, field.name)});
        } else {
            try stream.print(" {s}", .{@field(command.parameters, field.name)});
        }
    }
}

fn printCommand(stream: anytype, cmd: *langs.Command, comptime tag: langs.Command.Tag) !void {
    const CommandStruct = langs.Command.tagToType(tag);
    const casted = cmd.cast(CommandStruct).?;

    const ctype = CommandStruct.command_type;
    switch (ctype) {
        .lv2_command => try printCommandWithParams(stream, casted),
        .custom_command => try printCommandWithParams(stream, casted),
    }
}

pub fn printList(list: langs.CommandList, stream: anytype) !void {
    for (list.list.items) |cmd| {
        const command = @tagName(cmd.tag);
        try stream.print("{s}", .{command});

        switch (cmd.tag) {
            .load => {
                const load = cmd.cast(langs.Command.Load).?;
                try stream.print(" {s}", .{load.path});
            },
            .runqs => {
                const runqs = cmd.cast(langs.Command.RunQS).?;
                try stream.print(" {s}", .{runqs.program});
            },
            .noop, .quicksave => {},
            .rotate => {
                const rotate = cmd.cast(langs.Command.Rotate).?;
                try stream.print(" {d} {s}", .{ rotate.deg, rotate.bgfill });
            },

            .amp => try printCommand(stream, cmd, .amp),
            .rflanger => try printCommand(stream, cmd, .rflanger),
            .eq => try printCommand(stream, cmd, .eq),
            .phaser => try printCommand(stream, cmd, .phaser),
            .mbeq => try printCommand(stream, cmd, .mbeq),
            .chorus => try printCommand(stream, cmd, .chorus),
            .pitchscaler => try printCommand(stream, cmd, .pitchscaler),
            .reverb => try printCommand(stream, cmd, .reverb),
            .highpass => try printCommand(stream, cmd, .highpass),
            .delay => try printCommand(stream, cmd, .delay),
            .vinyl => try printCommand(stream, cmd, .vinyl),
            .revdelay => try printCommand(stream, cmd, .revdelay),
            .gate => try printCommand(stream, cmd, .gate),
            .detune => try printCommand(stream, cmd, .detune),
            .overdrive => try printCommand(stream, cmd, .overdrive),
            .degrade => try printCommand(stream, cmd, .degrade),
            .repsycho => try printCommand(stream, cmd, .repsycho),
            .talkbox => try printCommand(stream, cmd, .talkbox),
            .dyncomp => try printCommand(stream, cmd, .dyncomp),
            .thruzero => try printCommand(stream, cmd, .thruzero),
            .foverdrive => try printCommand(stream, cmd, .foverdrive),
            .gverb => try printCommand(stream, cmd, .gverb),
            .invert => try printCommand(stream, cmd, .invert),
            .tapedelay => try printCommand(stream, cmd, .tapedelay),
            .moddelay => try printCommand(stream, cmd, .moddelay),
            .multichorus => try printCommand(stream, cmd, .multichorus),
            .saturator => try printCommand(stream, cmd, .saturator),
            .vintagedelay => try printCommand(stream, cmd, .vintagedelay),

            .noise => try printCommand(stream, cmd, .noise),
            .wildnoise => try printCommand(stream, cmd, .wildnoise),
            .write => try printCommand(stream, cmd, .write),
            .embed => try printCommand(stream, cmd, .embed),
        }

        _ = try stream.write(";\n");
    }
}
