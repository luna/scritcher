scritcher command reference
---

scritcher scripts are shell-like commands separated by a semicolon.

## parameters of the arg type

some commands allow a `:N` syntax where N is an integer to use a given
argument when `scritcher` is called. so, for example, calling a script:
```
load :0;
showpath;
```

and then doing `scritcher path/to/script.scri arg` will print `arg` on the
screen and then exit.

## split and index arguments

plugin commands have a split and an index argument so that you can select
where in the file you want the plugin to be ran.

so, if you did `plugin 3 1...`, it would split the file in 3, then get the
part that is of index 1 (starting at 0).

**Keep in mind parts can start from either top or bottom of the image,
it depends of the file format**

## `load path_or_arg`

Load a file into memory. The file MUST end with the bmp extension (and so MUST
be a bmp. feel free to put other files, no warranties are given.)

## `amp split index gain`

Run the eg-amp plugin over the given slice of the file.

## `rflanger split index delay_depth_avg law_freq`

Run the Retro Flanger script from the SWH plugins.

Parameters:
 - `delay_depth_avg`: Average stall (ms), 0..10, default 2.5
 - `law_freq`: Flange frequency (Hz), 0.5..8, default 1

## `eq split index lo mid hi`

Run the DJ EQ plugin from the SWH plugins.

`lo`, `mid`, and `hi` are the respective dB gains for each frequency range.

All three ranges accept gains from -70dB to +6dB.
Default is 0 for all (no action taken).

## `phaser split index lfo_rate lfo_depth fb spread`

Run the LFO Phaser plugin from the SWH plugins.

Parameters:
 - `lfo_rate`: LFO Rate (Hz), 0..100, default 25
 - `lfo_depth`: LFO depth, 0..1, default 0.25
 - `fb`: Feedback, -1..1, default 0
 - `spread`: Spread (octaves), 0..2, default 1

## `mbeq split index band_1 band_2 band_3 band_4 band_5 band_6 band_7 band_8 band_9 band_10 band_11 band_12 band_13 band_14 band_15`

Multiband EQ from the SWH plugins.

In respective order, the band arugments represent the:
50Hz, 100Hz, 156Hz, 220Hz, 311Hz, 440Hz, 622Hz, 880Hz 1250Hz, 1750Hz, 2500Hz,
3500Hz, 5000Hz, 10000Hz and 20000Hz frequencies.

All of them represent the band's gain in dB. The range is -70 to +30dB,
default is 0.

## `chorus split index voices delay_base voice_spread detune law_freq attendb`

Multivoice Chrorus from the SWH plugins.

Parameters:
 - `voices`: Number of voices (int), 1..8, default 1
 - `delay_base`: Delay base (ms), 10..40, default 10
 - `voice_spread`: Voice separation (ms), 0-2, default 0.5
 - `detune`: Detune (%), 0..5, default 1
 - `law_freq`: LFO frequency (Hz), 2..30, default 9
 - `attendb`: Output attenuation (dB), -20..0, default 0

## `pitchscaler split index mult`

Runs the Higher Quality Pitch Scaler from the SWH plugins.

The `mult` parameter is the pitch coefficient, range from 0.5..2, default 1.

## `reverb split index roomLength roomWidth roomHeight sourceLR sourceFB listLR listFB hpf warmth diffusion`

Run the Early Reflection Reverb from the Invada Studio plugins.

Parameters:
 - `roomLength`: Room Length, 3..100, default 25
 - `roomWidth`: Room Length, 3..100, default 30
 - `roomHeight`: Room Length, 3..100, default 10
 - `sourceLR`: Source Pan, -1..1, default -0.01
 - `sourceFB`: Source (F/B), 0.5..1, default 0.8
 - `listLR`: Listener Pan, -1..1, default 0.01
 - `listFB`: Listener (F/B), 0..0.5, default 0.2
 - `hpf`: HPF (High Pass Filter), 20..2000, default 1000 (most likely you want 20)
 - `warmth`: Warmth, 0..100, default 50
 - `diffusion`: Diffusion, 0..100, default 50

## `highpass split index freq gain noClip`

Run the High Pass Filter from the Invada Studio plugins.

Parameters:
 - `freq`: Frequency. 20..20000, default 1000
 - `gain`: Gain, 0..12, default 0
 - `noClip`: Soft Clip (assumed boolean), 0..1, default 0

## `delay split index seed gain feedback_pc tap_count first_delay delay_range delay_scale delay_rand_pc gain_scale wet`

Parameters:
 - `seed`: Random seed, 0..1000, default 0
 - `gain`: Input gain (dB), -96..24, default 0
 - `feedback_pc`: Feedback (%), 0..100, default 0
 - `tap_count`: Number of taps, 2..128, default 2
 - `first_delay`: First delay (s), 0..5, default 0
 - `delay_range`: Delay range (s), 0..6, default 6
 - `delay_scale`: Delay change, 0..5, default 1
 - `delay_rand_pc`: Delay random (%), 0..100, default 0
 - `gain_scale`: Amplitude change, 0.2..5, default 1
 - `wet`: Dry/wet mix, 0..1, default 1

## `vinyl split index year warp click wear`

VyNil effect from SWH.

Parameters:
 - `year`: Year (int), 1900..1990, default 1990
 - `rpm`: RPM (int), 33..78, default 33
 - `warp`: Surface Warping, 0..1, default 0
 - `click`: Crackle, 0..1, default 0
 - `wear`: Wear, 0..1, default 0

## `revdelay split index delay_time dry_level wet_level feedback xfade_samp` 

Parameters:
 - `delay_time`: Delay time (s), 0..5, default 0
 - `dry_level`: Dry Level (dB), -70..0, default 0
 - `wet_level`: Wet Level (dB), -70..0, default 0,
 - `feedback`: Feedback, 0..1, default 0
 - `xfade_samp`: Crossfade samples (int), 0..5000, default 1250

## `wildnoise/noise split index seed repeat_bytes`

Inject random noise on the image.

the `wildnoise` version gives a harder version than `noise`.

Parameters:
 - `seed`, Random seed
 - `repeat_bytes`, Amount of bytes to preload with random data and repeat
    throughout the image slice

## `rotate deg bgfill`

Rotate the image by `deg` degrees, filling the resulting triangles with `bgfill`.

`bgfill` is a hex string, e.g `#000000`.

## `embed split index path_to_file`

embed an audio file in the given image. the file is opened with libsndfile,
which means some formats, like mp3, will not be opened.

## `quicksave`

Save the file on the same directory of the file specified by `load`, but
with a suffix on the filename (before extension).

Doing consecutive `quicksave`s will not overwrite any files, the suffixes will
just be different.

## `gate split index switch threshold attack hold decay gaterange` 

**TODO:** find good parameters

 - switch (bool): 0..1, default 0
 - threshold (dB): -70..12, default -70
 - attack (ms): 0.1..500, default 30
 - hold (ms): 5..3000, default 500
 - decay (ms): 5..4000, default 1000
 - gaterange (dB): -90..-20, default -90

## `detune split index detune mix output latency`

> A low-quality stereo pitch shifter for the sort of chorus and detune effects found on multi-effects hardware.

 - detune (cents, left channel is lowered in pitch, right channel is raised): 0..1, default 0.2
 - mix (wet/dry mix): 0..1, default 0.9
 - output (level trim): 0..1, default 0.5
 - latency (trade-off between latency and low-freq response): 0..1, default 0.5

other presets:
 - stereo detune: 0.2 0.9 0.5 0.5
 - out of tune: 0.8 0.7 0.5 0.5


## `overdrive split index drive muffle output`

> Possible uses include adding body to drum loops, fuzz guitar, and that 'standing outside a nightclub' sound. This plug does not simulate valve distortion, and any attempt to process organ sounds through it will be extremely unrewarding!

 - drive (amount of distortion): 0..1, default 0
 - muffle (gentle low-pass filter): 0..1, default 0
 - output (level trim): 0..1, default 0.5

## `degrade split index headroom quant rate post_filt non_lin output`

> Sample quality reduction

**NOTE:** finding the right parameters is HARD for this plugin.

 - headroom (peak clipping threshold): 0..1, default 0.8
 - quant (bit depth, typically 8 or below for "telephone" quality): 0..1, default 0.5
 - rate (sample rate): 0..1, default 0.65
 - post_filt (low-pass filter to muffle the distortion): 0..1, default 0.9
 - non_lin (additional harmonic distortion "thickening"): 0..1, default 0.58
 - output: 0..1, default 0.5

## `repsycho split index tune fine decay thresh hold mix quality`

**NOTE:** HARD to find good parameters

 - tune (coarse tune, semitones): 0..1, default 1
 - fine (fine tune, cents): 0..1, default 1
 - decay (adjust envelope of each trunk, a fast decay can be useful while setting up): 0..1, default 0.5
 - thresh (trigger level to divide the input into chunks): 0..1, default 0.6
 - hold (minimum chunk length): 0..1, default 0.45
 - mix (mix original signal with output): 0..1, default 1
 - quality (quality, bool. the high=1 setting uses smoother pitch-shifting and allows stereo): 0..1, default 0

## `talkbox split index wet dry carrier quality`

> High resolution vocoder

 - wet: 0..1, default 0.5
 - dry: 0..1, default 0
 - carrier: 0..1, default 0
 - quality: 0..1, default 1

## `dyncomp split index enable hold inputgain threshold ratio attack release gain_min gain_max rms`

 - enable (bool): 0..1, default 1
 - hold (bool): 0..1, default 0
 - inputgain (dB): -10..30, default 0
 - threshold (dB): -50..-10, default -30
 - ratio (???): 0..1, default 0
 - attack (seconds): 0.001..0.1, default 0.01
 - release (seconds): 0.03..3.0, default 0.3
 - gain\_min (dB): -20..40
 - gain\_max (dB): -20..40
 - rms (signal level, dB): -80..10
 
## `thruzero split index rate mix feedback depth_mod`

> Tape flanger and ADT

> This plug simulates tape-flanging, where two copies of a signal cancel out completely as the tapes pass each other. It can also be used for other "modulated delay" effects such as phasing and simple chorusing.

 - rate (modulation rate, set to minimum for static comb filtering): 0..1, default 0.3
 - mix (wet/dry mix, set to 50% for complete cancelling): 0..1, default 0.47
 - feedback (add positive or negative feedback for harsher or "ringing" sound): 0..1, default 0.3
 - depth_mod (modulation depth, set to less than 100% to limit build up of low frequencies with feedback): 0..1, default 1

## `foverdrive split index drive`

Fast Overdrive from SWH plugins.

 - drive: 1..3, default 1

## `gverb split index roomsize revtime damping drylevel earlylevel taillevel`

GVerb algorithm from SWH plugins.

 - roomsize (meters): 1..300, default 75.75
 - revtime (reverb time, seconds): 0.1..30, default 7.575
 - damping: 0..1, default 0.5
 - inputbandwidth: 0..1, default 0.75
 - drylevel (dB): -70..0, default 0
 - earlylevel (dB): -70..0, default 0
 - taillevel (dB): -70..0, default -17.5

## `invert split index`

> A utility plugin that inverts the signal, also (wrongly) known as a 180 degree phase shift.

## `tapedelay split index speed da_db t1d t1a_db...`

**TODO:** gives 0 output

> Correctly models the tape motion and some of the smear effect, there is no simulation fo the head saturation yet, as I don't have a good model of it. When I get one I will add it.

> The way the tape accelerates and decelerates gives a nicer delay effect for many purposes.

 - speed (inches/sec, 1=normal): 0..10, default 1
 - da\_db (dry level, dB): -90..0, default -90
 - t1d (tap 1 distance, inches): 0..4, default 0
 - t1a\_db (tap 1 level, dB): -90..0, default 0
 - t2d (tap 2 distance, inches): 0..4, default 1
 - t2a\_db (tap 2 level, dB): -90..0, default -90
 - t3d (tap 3 distance, inches): 0..4, default 2
 - t3a\_db (tap 3 level, dB): -90..0, default -90
 - t4d (tap 4 distance, inches): 0..4, default 3
 - t4a\_db (tap 4 level, dB): -90..0, default -90

## `moddelay split index base`

> A delay whose tap can be modulated at audio rate.

 - base (base delay, seconds): 0..1, default 1

## `multichorus split index min_delay mod_depth mod_rate stereo voices vphase amount dry freq freq2 q overlap level_in level_out lfo`

Calf Multi Chorus

 - `min_delay` (ms): 0.1..10, default 5
 - `mod_depth` (ms): 0.1..10, default 6
 - `mod_rate` (hz): 0.1..20, default 0.1
 - `stereo` (degrees): 0..360, default 180
 - `voices`: 1..8, default 4
 - `vphase` (inter-voice phase, degrees): 0..360, default 64
 - `amount`: 0..4, default 0.5
 - `dry`: 0..4, default 0.5
 - `freq` (center frq 1, hz): 10..20000, default 100
 - `freq2` (center frq 2, hz): 10..20000, default 5000
 - `q` (???): 0.125..8, default 0.125
 - `overlap`: 0..1, default 0.75
 - `level_in` (Input Gain): 0.0156250..64, default 1
 - `level_out` (Output Gain): 0.0156250..64, default 1
 - `lfo` (toggle): 0..1, default 1

## `saturator split index bypass level_in level_out mix drive blend lp_pre_freq hp_pre_fre lp_post_freq hp_post_freq p_freq p_level p_q pre post`

 - `bypass` (toggle): 0..1, default 0
 - `level_in` (Input Gain): 0.0156250..64, default 1
 - `level_out` (Output Gain): 0.0156250..64, default 1
 - `mix`: 0..1, default 1
 - `drive` (saturation, coef): 0.1..10, default 5
 - `blend` (coef): -10..10, default 10
 - `lp_pre_freq` (lowpass, hz): 10..20000, default 20000
 - `hp_pre_freq` (highpass, hz): 10..20000, default 10
 - `lp_post_freq` (lowpass, hz): 10..20000, default 20000
 - `hp_post_freq` (highpass, hz): 10..20000, default 10
 - `p_freq` (Tone, hz): 80..8000, default 2000
 - `p_level` (Amount): 0.0625..16, default 1
 - `p_q` (???, coef): 0.1..10, default 1
 - `pre` (Activate Pre, toggle): 0..1, default 0
 - `post` (Activate Post, toggle): 0..1, default 0

## `vintagedelay split index ...`

 - `level_in` (Input Gain): 0.0156250..64, default 1
 - `level_out` (Output Gain): 0.0156250..64, default 1
 - `subdiv` (int): 1..16, default 4
 - `time_l` (int): 1..16, default 3
 - `time_r` (int): 1..16, default 5
 - `feedback`: 0..1, default 0.5
 - `amount` (Wet): 0..4, default 0.25
 - `mix_mode` (enum): Stereo=0, Ping-Pong=1, L then R=2, R then L=3, default 1
 - `medium` (enum): Plain=0, Tape=1, Old Tape=2, default 1
 - `dry` (dry): 0..4, default 1
 - `width` (stereo width, strict): -1..1, default 1
 - `fragmentation` (enum): Repeating=0, Pattern=1, default 0
 - `pbeats` (Pattern Beats, int): 1..8, default 4
 - `pfrag` (Pattern Fragmentation, int): 1..8, default 4
 - `timing` (enum): BPM=0, ms=1, Hz=2, Sync=3, default 0
 - `bpm`: 30..300, default 120
 - `ms` (int): 10..2000, default 500
 - `hz`: 0.01..100, default 2
 - `bpm_host` (strict): 1..300, default 120
